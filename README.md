# Features, Improvements, and Bugs

This is a place to suggest features and improvements for, and report bugs in gitlapp. If there already is an issue that tackles your concern, consider voting on that issue rather than creating a new one.
